﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace OwinOAuth2.ASP.Extensions
{
	public static class OwinContextExtensions
	{
		public static string GetUserId(this IOwinContext ctx)
		{
			string result = "-1";
			Claim claim = ctx.Authentication.User.Claims.FirstOrDefault(c => c.Type == "UserID");
			if (claim != null)
			{
				result = claim.Value;
			}
			return result;
		}
	}
}
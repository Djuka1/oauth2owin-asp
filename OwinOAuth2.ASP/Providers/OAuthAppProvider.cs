﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using OwinOAuth2.ASP.Models;
using OwinOAuth2.ASP.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace OwinOAuth2.ASP.Providers
{
	public class OAuthAppProvider : OAuthAuthorizationServerProvider
	{
		public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
		{
			return Task.Factory.StartNew(() =>
			{
				string username = context.UserName;
				string password = context.Password;
				UserService userService = new UserService();
				User user = userService.GetUserByCredentials(username, password);
				if (user != null)
				{
					List<Claim> claims = new List<Claim>()
					{
						new Claim(ClaimTypes.Name, user.Name),
						new Claim("UserID", user.Id)
					};

					ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims, Startup.OAuthOptions.AuthenticationType);
					context.Validated(new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties() { }));
				}
				else
				{
					context.SetError("invalid_grant", "Error");
				}

			});
		}

		public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
		{
			if (context.ClientId == null)
			{
				context.Validated();
			}
			return Task.FromResult<object>(null);
		}
	}
}

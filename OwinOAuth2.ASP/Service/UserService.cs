﻿using OwinOAuth2.ASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OwinOAuth2.ASP.Service
{
	public class UserService
	{
		// Hardcoded as if user was selected from a database ...
		public User GetUserByCredentials(string email, string password)
		{
			if (email != "email@domain.com" || password != "password")
			{
				return null;
			}

			User user = new User() { Id = "1", Email = "email@domain.com", Password = "password", Name = "John Doe" };
			if(user != null)
			{
				user.Password = string.Empty;
			}
			return user;
		}
	}
}